using System;

namespace Test
{
    class Program
    {
        static void Main(string[] args)
        {
            string enterText = "hii";
            Reverse(enterText);
            GetReversedText(enterText);
            SpinWords(enterText);
            Console.ReadKey();
            

        }
        static void GetReversedText(string str)
        {
            Console.Write("Введите сообщение: ");
            string name = Console.ReadLine();
            //string name = "Tomiris";
            int counter = name.Length;//3
            //string str;
            
            for (int i = 0; i < name.Length; i++)
            {
                counter--;
                // 0 1 2 = 3 -1 2 1 0
                //2 1 0
                //t o m
                char[] symbols = { name[counter] };
                str = new string(symbols);
                //Console.WriteLine(symbols); //2 1 0
                ;
                Console.WriteLine(str);
            }

        }
        static void Reverse(string text) {
            string name = "Tom";
            Console.WriteLine(text+name);
        }
        static string SpinWords(string sentence)
        {
            string[] words = sentence.Split(' ');
            string str = "";

            for (int i = 0; i < words.Length; i++)
            {
                if (words[i].Length >= 5)
                {
                    char[] s = words[i].ToCharArray();
                    Array.Reverse(s);
                    string s2 = new string(s);
                    words[i] = s2;
                }
                str = str + words[i] + " ";
            }

            Console.WriteLine(str);

            return str;

        }
    }
}
